﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace derivco
{
    public class Node : IEnumerable<Node>
    {
        private readonly Dictionary<string, Node> _children = new Dictionary<string, Node>();
        public readonly string Name;
        public Node Parent { get; private set; }

        public Node(string name)
        {
            Name = name;
        }

        //Find the First child nod
        public Node GetChild(string name)
        {
            if (_children.ContainsKey(name))
            {
                return _children[name];
            }

            throw new Exception("Child Not found");
        }

        //Add a new node to the current node
        public void Add(Node item)
        {
            item.Parent?._children.Remove(item.Name);
            item.Parent = this;

            _children.Add(item.Name, item);

        }

        // Get the number sibblings.
        public int Count => _children.Count;

        public IEnumerator<Node> GetEnumerator()
        {
            return _children.Values.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        //Build the tree from a string 
        public static Node BuildTree(string tree)
        {
            var lines = tree.Split(new[] { Environment.NewLine },
                StringSplitOptions.RemoveEmptyEntries);

            //since a tree needs to have only one root node this node us needed to allow having multiple menus at root position.
            var rootNode = new Node("Root");
            var sibblings = new List<Node> { rootNode };

            foreach (var line in lines)
            {
                var trimmedLine = line.Trim();
                var indent = line.Length - trimmedLine.Length;

                var child = new Node(trimmedLine);
                sibblings[indent].Add(child);

                if (indent + 1 < sibblings.Count)
                {
                    sibblings[indent + 1] = child;
                }
                else
                {
                    sibblings.Add(child);
                }
            }

            return rootNode;

        }
    }
}