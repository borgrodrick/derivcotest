﻿using System;
using System.Linq;

namespace derivco
{
    class Program
    {
        static void Main(string[] args)
        {
            //tree representation where each submenu is split by a space 
            var tree = Node.BuildTree(@"
Administration
 User Admin
  Create User
  Edit User
 Product Admin
  All Products
   My Product
  Create Product
 Other Admin
  Order Reports
   Audit Reports
    Updated Orders
    Created Orders
  Create Order
Reports
  Win Tech Report
  Microsoft Report");

            PrintTree(tree, "");
            Console.ReadKey();
        }

        public static void PrintTree(Node tree, String indent)
        {
            Console.WriteLine(indent + "-" + tree.Name);
            indent += "     " ;

            for (int i = 0; i < tree.Count; i++)
            {
                PrintTree(tree.ElementAt(i), indent);
            }

        }
    }
}
